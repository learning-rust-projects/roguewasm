import * as wasm from './roguewasm_bg.wasm';

const heap = new Array(32).fill(undefined);

heap.push(undefined, null, true, false);

function getObject(idx) { return heap[idx]; }

let heap_next = heap.length;

function dropObject(idx) {
    if (idx < 36) return;
    heap[idx] = heap_next;
    heap_next = idx;
}

function takeObject(idx) {
    const ret = getObject(idx);
    dropObject(idx);
    return ret;
}

const lTextDecoder = typeof TextDecoder === 'undefined' ? (0, module.require)('util').TextDecoder : TextDecoder;

let cachedTextDecoder = new lTextDecoder('utf-8', { ignoreBOM: true, fatal: true });

cachedTextDecoder.decode();

let cachegetUint8Memory0 = null;
function getUint8Memory0() {
    if (cachegetUint8Memory0 === null || cachegetUint8Memory0.buffer !== wasm.memory.buffer) {
        cachegetUint8Memory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachegetUint8Memory0;
}

function getStringFromWasm0(ptr, len) {
    return cachedTextDecoder.decode(getUint8Memory0().subarray(ptr, ptr + len));
}

function addHeapObject(obj) {
    if (heap_next === heap.length) heap.push(heap.length + 1);
    const idx = heap_next;
    heap_next = heap[idx];

    heap[idx] = obj;
    return idx;
}

function _assertClass(instance, klass) {
    if (!(instance instanceof klass)) {
        throw new Error(`expected instance of ${klass.name}`);
    }
    return instance.ptr;
}

let WASM_VECTOR_LEN = 0;

const lTextEncoder = typeof TextEncoder === 'undefined' ? (0, module.require)('util').TextEncoder : TextEncoder;

let cachedTextEncoder = new lTextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length);
        getUint8Memory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len);

    const mem = getUint8Memory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3);
        const view = getUint8Memory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}
/**
*/
export class Engine {

    static __wrap(ptr) {
        const obj = Object.create(Engine.prototype);
        obj.ptr = ptr;

        return obj;
    }

    free() {
        const ptr = this.ptr;
        this.ptr = 0;

        wasm.__wbg_engine_free(ptr);
    }
    /**
    * @param {any} display
    */
    constructor(display) {
        var ret = wasm.engine_new(addHeapObject(display));
        return Engine.__wrap(ret);
    }
    /**
    * @param {number} x
    * @param {number} y
    * @param {number} val
    */
    on_dig(x, y, val) {
        wasm.engine_on_dig(this.ptr, x, y, val);
    }
    /**
    */
    draw_map() {
        wasm.engine_draw_map(this.ptr);
    }
    /**
    * @param {number} x
    * @param {number} y
    */
    redraw_at(x, y) {
        wasm.engine_redraw_at(this.ptr, x, y);
    }
    /**
    * @param {number} x
    * @param {number} y
    */
    place_box(x, y) {
        wasm.engine_place_box(this.ptr, x, y);
    }
    /**
    * @param {PlayerCore} pc
    * @param {number} x
    * @param {number} y
    */
    open_box(pc, x, y) {
        _assertClass(pc, PlayerCore);
        wasm.engine_open_box(this.ptr, pc.ptr, x, y);
    }
    /**
    * @param {number} x
    * @param {number} y
    */
    mark_wasmprize(x, y) {
        wasm.engine_mark_wasmprize(this.ptr, x, y);
    }
    /**
    * @param {PlayerCore} pc
    * @param {number} x
    * @param {number} y
    */
    move_player(pc, x, y) {
        _assertClass(pc, PlayerCore);
        wasm.engine_move_player(this.ptr, pc.ptr, x, y);
    }
    /**
    * @param {number} x
    * @param {number} y
    * @returns {boolean}
    */
    free_cell(x, y) {
        var ret = wasm.engine_free_cell(this.ptr, x, y);
        return ret !== 0;
    }
}
/**
*/
export class PlayerCore {

    static __wrap(ptr) {
        const obj = Object.create(PlayerCore.prototype);
        obj.ptr = ptr;

        return obj;
    }

    free() {
        const ptr = this.ptr;
        this.ptr = 0;

        wasm.__wbg_playercore_free(ptr);
    }
    /**
    * @param {number} x
    * @param {number} y
    * @param {string} icon
    * @param {string} color
    * @param {any} display
    */
    constructor(x, y, icon, color, display) {
        var ptr0 = passStringToWasm0(icon, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        var ptr1 = passStringToWasm0(color, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len1 = WASM_VECTOR_LEN;
        var ret = wasm.playercore_new(x, y, ptr0, len0, ptr1, len1, addHeapObject(display));
        return PlayerCore.__wrap(ret);
    }
    /**
    * @returns {number}
    */
    x() {
        var ret = wasm.playercore_x(this.ptr);
        return ret;
    }
    /**
    * @returns {number}
    */
    y() {
        var ret = wasm.playercore_y(this.ptr);
        return ret;
    }
    /**
    */
    draw() {
        wasm.playercore_draw(this.ptr);
    }
    /**
    * @param {number} x
    * @param {number} y
    */
    move_to(x, y) {
        wasm.playercore_move_to(this.ptr, x, y);
    }
    /**
    */
    emit_stats() {
        wasm.playercore_emit_stats(this.ptr);
    }
    /**
    * @param {number} hits
    * @returns {number}
    */
    take_damage(hits) {
        var ret = wasm.playercore_take_damage(this.ptr, hits);
        return ret;
    }
}

export const __wbg_alert_dcd7d0308629477d = function(arg0, arg1) {
    alert(getStringFromWasm0(arg0, arg1));
};

export const __wbg_draw_4db0c169087cf61d = function(arg0, arg1, arg2, arg3, arg4) {
    getObject(arg0).draw(arg1, arg2, getStringFromWasm0(arg3, arg4));
};

export const __wbg_draw_b9bd6eee723ae3e6 = function(arg0, arg1, arg2, arg3, arg4, arg5, arg6) {
    getObject(arg0).draw(arg1, arg2, getStringFromWasm0(arg3, arg4), getStringFromWasm0(arg5, arg6));
};

export const __wbindgen_object_drop_ref = function(arg0) {
    takeObject(arg0);
};

export const __wbindgen_throw = function(arg0, arg1) {
    throw new Error(getStringFromWasm0(arg0, arg1));
};

